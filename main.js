//je déclare mes variables
let randomNumber;
let tries;
randomNumber = Math.floor(Math.random() * 100);
  
//je récupere les élèments du DOM par leur ID
const form = document.getElementById("guess_form");
let meilleurScore = localStorage.getItem('MeilleurScore');
const meilleurScoreElement = document.getElementById("MeilleurScore-value");

// condition pour mettre à jour le meilleur score
 if (meilleurScore != null) {
  meilleurScoreElement.textContent = meilleurScore;
 }

//fonction pour démarrer le jeu
window.addEventListener("DOMContentLoaded", function (event) {
  function startGame() {
    tries = 1;
    randomNumber = Math.floor(Math.random() * 100);
    console.log("Le nombre à deviner est :" + randomNumber);
  }
  startGame();
});

//fonction pour générer le formulaire
function submitForm(event) {
  event.preventDefault();
  const value = form.guess.value;
  console.log("value" + value);
  
 //condition si la valeur est plus petite que le chiffre ça affichera le nombre est trop petit 
  if (value < randomNumber) {
    let p1 = document.getElementById("p1");
    p1.textContent = "Le nombre est trop petit !";
    tries = tries +1;
  // si la valeur est plus grande ça affichera le nombre est trop grand
  } else if (value > randomNumber) {
    let p1 = document.getElementById("p1");
    p1.textContent = "Le nombre est trop grand !";
    tries = tries +1;
  // si la valeur est égale au nombre ça affichera vous avez gagné
  } else {
    let p1 = document.getElementById("p1");
    p1.textContent = " Bravo Vous avez gagné !"
 //récupere le meilleur score depuis le storage et l'affiche  
    localStorage.setItem("MeilleurScore", tries);
	
	}
  }
  console.log("tries" + tries);

//écoute l'évènement de soumission du formulaire
form.addEventListener("submit", submitForm);
